import { User } from './user.entity';
import * as bcrypt from 'bcryptjs';

describe('UserEntity', () => {
  let user;

  beforeEach(() => {
    user = new User();
    user.password = 'testPassword';
    user.salt = 'testSalt';

    bcrypt.hash = jest.fn();
  });
  describe('validatePassword', () => {
    it('valid password chack', async () => {
      bcrypt.hash.mockResolvedValue('testPassword');
      const result = await user.validatePassword('123456');

      expect(bcrypt.hash).toHaveBeenCalled();
      expect(bcrypt.hash).toHaveBeenCalledWith('123456', 'testSalt');
      expect(result).toEqual(true);
    });
    it('invalid password chack', async () => {
      bcrypt.hash.mockResolvedValue('testHash');
      const result = await user.validatePassword(user.password);

      expect(result).toEqual(false);
    });
  });
});
