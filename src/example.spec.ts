class FriendList {
  friends = [];

  addFriend(name: string) {
    this.friends.push(name);
    this.announceFriend(name);
  }

  announceFriend(name) {
    // global.console.log(`${name} is now a friend !`);
  }

  removeFriend(name) {
    const idx = this.friends.indexOf(name);

    if (idx === -1) {
      throw new Error('Friend not found! ');
    }

    this.friends.splice(idx, 1);
  }
}
describe('FriendList', () => {
  let friendList;

  beforeEach(() => {
    friendList = new FriendList();
  });

  it('initializes friend list', () => {
    expect(friendList.friends.length).toEqual(0);
  });

  it('adding friend list', () => {
    friendList.addFriend('hemant');

    expect(friendList.friends.length).toEqual(1);
  });

  it('announces friendship', () => {
    friendList.announceFriend = jest.fn();
    expect(friendList.announceFriend).not.toHaveBeenCalled();

    friendList.addFriend('hemant');
    expect(friendList.announceFriend).toHaveBeenCalledWith('hemant');
  });

  it('announces friendship', () => {
    friendList.announceFriend = jest.fn();
    expect(friendList.announceFriend).not.toHaveBeenCalled();

    friendList.addFriend('hemant');
    expect(friendList.announceFriend).toHaveBeenCalledWith('hemant');
  });

  describe('remove friend', () => {
    it('removing a friend', () => {
      friendList.addFriend('hemant');
      expect(friendList.friends[0]).toEqual('hemant');
      friendList.removeFriend('hemant');
      expect(friendList.friends[0]).toBeUndefined();
    });
    it('throwing the exception', () => {
      expect(() => friendList.removeFriend('hemant')).toThrow(
        new Error('Friend not found! '),
      );
    });
  });
});
